# kubernetes-projeto2-dio



## Getting started

# PROJETO DIO DEPLOY APPLICATION

**Procedimento utilizado para criar o projeto , backend , frontend autodeploy**
- Atenção: estar conectado ao docker hub para poder subir o projeto ao rodar o script.sh.

**Usando a aws para cluster kubernetes EKS** 

aws eks --region sa-east-1 describe-cluster --name dio-cluster-aws --query cluster.status
aws eks --region sa-east-1 update-kubeconfig --name dio-cluster-aws (se flhar renomear o config original e roda denovo)

- kubectl get svc - se deu certo
- kubectl get nodes --watch


**Usando GCP com cluster kubernetes**
- instalar o cli da google gke
- Install-Module GoogleCloud
- Set-ExecutionPolicy RemoteSigned
- glcoud components install gke-gcloud-auth-plugin
- gloud auth login
- gloud auth list

linha de comando gcloud para criar o cluster na gcp
---------------------------------------------------
gcloud beta container --project "dio-projects" clusters create "cluster-1" --zone "us-central1-c" --no-enable-basic-auth --cluster-version "1.24.7-gke.900" --release-channel "regular" --machine-type "e2-small" --image-type "COS_CONTAINERD" --disk-type "pd-balanced" --disk-size "10" --metadata disable-legacy-endpoints=true --scopes "https://www.googleapis.com/auth/devstorage.read_only","https://www.googleapis.com/auth/logging.write","https://www.googleapis.com/auth/monitoring","https://www.googleapis.com/auth/servicecontrol","https://www.googleapis.com/auth/service.management.readonly","https://www.googleapis.com/auth/trace.append" --max-pods-per-node "110" --num-nodes "2" --logging=SYSTEM,WORKLOAD --monitoring=SYSTEM --enable-ip-alias --network "projects/dio-projects/global/networks/default" --subnetwork "projects/dio-projects/regions/us-central1/subnetworks/default" --no-enable-intra-node-visibility --default-max-pods-per-node "110" --no-enable-master-authorized-networks --addons HorizontalPodAutoscaling,HttpLoadBalancing,GcePersistentDiskCsiDriver --enable-autoupgrade --enable-autorepair --max-surge-upgrade 1 --max-unavailable-upgrade 0 --enable-shielded-nodes --node-locations "us-central1-c"


linha de comando para criar uma vm bastian
---------------------------------------------------
gcloud compute instances create bastian-1 --project=dio-projects --zone=us-central1-c --machine-type=e2-small --network-interface=network-tier=PREMIUM,subnet=default --maintenance-policy=MIGRATE --provisioning-model=STANDARD --service-account=824771621031-compute@developer.gserviceaccount.com --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append --create-disk=auto-delete=yes,boot=yes,device-name=bastian-1,image=projects/debian-cloud/global/images/debian-11-bullseye-v20221206,mode=rw,size=10,type=projects/dio-projects/zones/us-central1-a/diskTypes/pd-balanced --no-shielded-secure-boot --shielded-vtpm --shielded-integrity-monitoring --reservation-affinity=any


## depois de criado o cluster e ter feito o login pelo cli 
clicar no : botao e em conectar , copiar o comando
gcloud container get-credentials cluster-1 --zone us-central-c --project dio-projects 
gcloud container clusters get-credentials cluster-1 --zone us-central1-c --project dio-projects

pegar a chave criada
ssh-rsa ..... gcp

usar essa chave na bastian em ssh
- adquirir o ip da maquina 34.136.94.236 ip externo
10.128.0.7
EXTERNAL_IP: 34.136.94.236
depois de conectado com gcp, 
gcloud config set account djchristiandf@gmail.com
gcloud init
- gloud auth login
//- gloud auth list
- sudo apt-get install kubectl
- sudo apt-get install google-cloud-sdk-gke-gcloud-auth-plugin
- sudo apt-get install git
gcloud container clusters get-credentials cluster-1 --zone us-central1-c --project dio-projects

criando secrets
kubectl apply -f .\secrets.yml
kubectl get secrets


